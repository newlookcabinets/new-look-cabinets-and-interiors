With painting or refinishing services from New Look Cabinets, you can expect nothing but the best. To us, nothing is more important than ensuring our customers fall in love with their product. 

Address: 2850 107 Ave SE, #105, Calgary, Alberta T2Z 3R7, Canada

Phone: 403-719-7246

Website: https://www.newlookcabinets.com/painters-in-calgary
